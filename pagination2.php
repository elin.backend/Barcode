<?php
include("connection.php");


global $conn;
/*$tbl_name="audit_trail";	

$query = "SELECT COUNT(*) as num FROM $tbl_name";
$total_pages = mysql_fetch_array(mysql_query($query));
$total_pages = $total_pages['num'];
$targetpage = "home.php"; 	
                        //how many items to show per page*/
							
/* Get data. */
$result = mysqli_query($conn,$sql);

/* Setup page vars for display. */
if ($page == 0) $page = 1;					//if no page var is given, default to 1.
$prev = $page - 1;							//previous page is page - 1
$next = $page + 1;							//next page is page + 1
$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
$lpm1 = $lastpage - 1;						//last page minus 1

/* 
    Now we apply our rules and draw the pagination object. 
    We're actually saving the code to a variable in case we want to draw it more than once.
*/
$pagination = "";
if($lastpage > 1)
{	
    $pagination .= "<ul class=pagination>";
    //previous button
    if ($page > 1) 
        $pagination.= "<li><a href=\"$targetpage&page=$prev\"><< previous</a>";
    else
        $pagination.= "<li><span class=\"disabled\"><< previous</span>";	

    //pages	
    if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
    {	
        for ($counter = 1; $counter <= $lastpage; $counter++)
        {
            if ($counter == $page)
                $pagination.= "<span class=\"current\">$counter</span>";
            else
                $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a>";					
        }
    }
    elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
    {
        //close to beginning; only hide later pages
        if($page < 1 + ($adjacents * 2))		
        {
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li><span class=\"current\">$counter</span>";
                else
                    $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a>";					
            }
            $pagination.= "<li>...";
            $pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a>";
            $pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a>";		
        }
        //in middle; hide some front and some back
        elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
        {
            $pagination.= "<li><a href=\"$targetpage&page=1\">1</a>";
            $pagination.= "<li><a href=\"$targetpage&page=2\">2</a>";
            $pagination.= "<li>...";
            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a>";					
            }
            $pagination.= "<li>...";
            $pagination.= "<li><a href=\"$targetpage&page=$lpm1\">$lpm1</a>";
            $pagination.= "<li><a href=\"$targetpage&page=$lastpage\">$lastpage</a>";		
        }
        //close to end; only hide early pages
        else
        {
            $pagination.= "<li><a href=\"$targetpage&page=1\">1</a>";
            $pagination.= "<li><a href=\"$targetpage&page=2\">2</a>";
            $pagination.= "<li>...";
            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li><span class=\"current\">$counter</span>";
                else
                    $pagination.= "<li><a href=\"$targetpage&page=$counter\">$counter</a>";					
            }
        }
    }

    //next button
    if ($page < $counter - 1) 
        $pagination.= "<li><a href=\"$targetpage&page=$next\">next >></a>";
    else
        $pagination.= "<li><span class=\"disabled\">next >></span>";
    $pagination.= "</ul>\n";		
}
    


?>
