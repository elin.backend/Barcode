<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Barcode Management</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/full-width-pics.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/timeline.css" rel="stylesheet">
    <script src="js/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>  

</head>

<body class="main-body">
    <?php include('navigation.php');?>
        <div class="container">
        <?php 
        require_once('connection.php');
        session_start();
        if($_SESSION['user']==null){
            echo "<h1>You are not Allowed to View page</h1>";
        }
        else{
        include("navigation.php");
        ?>
        <div class="row"><br>&nbsp;<br>&nbsp;<br>&nbsp;<br></div>
        <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row"> 
                        <div class="col-md-8">
                        <!-- <h3>
                            <div class="row">
                            &nbsp;&nbsp;Batch #<?php 
                            $g = $_GET['id'];
                            echo $g."&nbsp;</div><div>Date:";
                            if(!empty($_GET['id'])){
                                $q = "SELECT * FROM uniquebar WHERE Batch='$g' LIMIT 1";
                                if($result = $conn->query($q)){
                                    if(mysqli_num_rows($result)){
                                    $row = mysqli_fetch_array($result);
                                    echo $row['Date']."</div>";
                                    }
                                }
                            }
                            
                            ?></h3> -->
                            <h3>Department List</h3>
                        </div>
                        <div class="col-md-2" style="margin-left:32px;">
                        <a href="add_dept.php" class="btn btn-success pull-left btn-xs">Add Department</a>
                        </div>

                        <div class="col-md-1" style="">
                        <!-- <a href=""><i class="fa fa-back"></i></a> -->
                        <!-- <a href="home.php" class="btn btn-primary pull-right" >Back to Batches</a> -->
                        <a href="home.php" class="pull-right"><i class="fa fa-reply"></i></a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-body">
                <div class="col-md-12">
                    <table class="table table-success table-hover table-responsive" id="batch-tbl">
                    <thead>
                        <th>Dept ID</th>
                        <th>Dept Name</th>
                        <th>Dept ABBR</th>
                        <th>Action</th>
                    </thead>
                    <tbody>

            <?php
            // $iddd = mysqli_real_escape_string($conn,$_GET['id']);
            $count = $conn->query("SELECT count(*) as counter FROM department"); 
            if(mysqli_num_rows($count)){
            $row = mysqli_fetch_object($count);
            // print_r($row);
            $count = $row->counter;
            // print_r($count);
        }
            $tbl_name="department";	
            $total_pages = $count;
            $tp = basename($_SERVER['REQUEST_URI']);
            $tp1 = explode('&',$tp);
            $targetpage = $tp1[0];
            $adjacents = 3;
            $limit = 15; 		
            $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 0;  
        if($page) 
            $start = ($page - 1) * $limit; 			
        else
            $start = 0;
            $sql = "SELECT * FROM $tbl_name LIMIT $start, $limit";

            include('pagination2.php');
             if(mysqli_num_rows($result)>0){
                $i=1;
                while($row =mysqli_fetch_array($result)){
                ?>
                    <tr>
                        <td><?= $i++;?></td>
                        <td><?= $row['DPT_NAME'];?></td>
                        <td><?= $row['DPT_ABBR'];?></td>
                        <td><a href="update_dept.php?id=<?= $row['DPT_ID'];?>" class="btn btn-success">update</a></td>
                    </tr>
                <?php
                }
            }else{
                echo "<tr><td colspan=4>No Records Found</td></tr>";
            }
            ?>
                    </tbody>
                </table>

            </div>
                </div>
                <div class="panel panel-footer">
                <div class="text-center">
                     <?php echo $pagination; ?>   
                </div> 
                </div>
          
            </div>
        </div>
        </div>
        <?php }?>
        </div>
    
</body>

</html>
