<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Barcode Management</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/full-width-pics.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/timeline.css" rel="stylesheet">
    <script src="js/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>  

</head>

<body class="main-body">
    <?php include('navigation.php');?>
        <div class="container">
        <?php 
        require_once('connection.php');
        session_start();
        if($_SESSION['user']==null){
            echo "<h1>You are not Allowed to View page</h1>";
        }
        else{
        include("navigation.php");
        ?>
        <div class="row"><br>&nbsp;<br>&nbsp;<br>&nbsp;<br></div>
        <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row"> 
                        <div class="col-md-8">
                        <!-- <h3>
                            <div class="row">
                            &nbsp;&nbsp;Batch #<?php 
                            $g = $_GET['id'];
                            echo $g."&nbsp;</div><div>Date:";
                            if(!empty($_GET['id'])){
                                $q = "SELECT * FROM uniquebar WHERE Batch='$g' LIMIT 1";
                                if($result = $conn->query($q)){
                                    if(mysqli_num_rows($result)){
                                    $row = mysqli_fetch_array($result);
                                    echo $row['Date']."</div>";
                                    }
                                }
                            }
                            
                            ?></h3> -->
                            <h3>Update Department</h3>
                        </div>
                        <div class="col-md-2" style="margin-left:32px;">
                        <!-- <a class="btn btn-success pull-left btn-xs"  data-toggle="modal" data-target="#exampleModal">Add Department</a> -->
                        </div>

                        <div class="col-md-1" style="">
                        <!-- <a href=""><i class="fa fa-back"></i></a> -->
                        <!-- <a href="home.php" class="btn btn-primary pull-right" >Back to Batches</a> -->
                        <a href="dept.php?" class="pull-right"><i class="fa fa-reply"></i></a>
                        </div>
                    </div>
                </div>
                <?php
                    // require_once('barcodeC.php');
                    $iddd = mysqli_real_escape_string($conn,$_GET['id']);
                        // echo $iddd;
                        $sql = "SELECT * FROM department WHERE DPT_ID = '$iddd'"; 
                    $result = mysqli_query($conn,$sql);
                    // print_r ($result);
                    $row =mysqli_fetch_array($result);
                    // print_r ($row["DPT_ABBR"]);
                ?>

                <?php 
                    if(isset($_POST['saveBtn'])){
                        $deptName = $_POST['dept_name'];
                        $deptABBR = $_POST['dept_abbr'];
                        // $iddd = mysqli_real_escape_string($conn,$_GET['id']);
                        // echo $iddd;
                        $update = mysqli_query($conn, "UPDATE `department` Set
                            DPT_NAME = '".$deptName."',
                            DPT_ABBR = '".$deptABBR."'
                        WHERE DPT_ID = '".$iddd."'
                                ");
                            
                        if($update){
                                $response = array('status' => 'SUCCESS',
                                'message' => 'UPDATE SUCCESS'
                                );
                            // echo json_encode($response);
                            ?>
                            <div style="padding:25px 90px 0 90px;">
                                <div class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Success! </strong> Department successfully updated.
                                </div>
                            </div>
                            <?php
                        }else{
                            $response = array('status' => 'ERROR',
                                                'message' => 'INSERTING DATA FAIL');
                            // echo json_encode($response);
                        }
                    }
                ?>
                <div class="panel panel-body">
                    <div class="col-md-12" style="padding:0 90px 0 90px">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype='multipart/form-data'>
                            <div class='form-group'>
                                <label>Department Name</label>
                                <input name="dept_name" type='text' class="form-control" placeholder=" Department name" value="<?= $row["DPT_NAME"];?>"/>
                            </div>
                            <div class='form-group'>
                                <label>Department Example</label>
                                <input name="dept_abbr" type='text' class="form-control" placeholder=" Department" value="<?= $row["DPT_ABBR"];?>"/>
                            </div>
                            <button name="saveBtn" class="btn btn-primary pull-right" type='submit'>Save changes</button>
                        </form>
                    </div>
                </div>
                <div class="panel panel-footer">
                <div class="text-center">
                     <?php echo $pagination; ?>
                </div> 
                </div>

            </div>
        </div>
        </div>
        <?php }?>
        </div>
    
</body>

</html>
