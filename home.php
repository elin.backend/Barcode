<?php 
    session_start();
?>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Barcode Management</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/full-width-pics.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="css/timeline.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="plugins/datatables/dataTables.bootstrap.css">
        <script type="text/javascript" src="js/jQuery-2.1.4.min.js"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
    </head>

    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 style="">Login</h4>
            <p>You need to login first to access this module</p>
            </div>
            <div class="modal-body" style="padding:40px 90px;">
            <div id="success" class="alert alert-success hide alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Success! </strong> Login successfull.
            </div>
            <div id="failed" class="alert alert-danger hide alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>Failed! </strong> User name or password is incorrect.
            </div>
                <div class='form-group' >
                    <label>User Name</label>
                    <input id="user_name" type='text' class="form-control" placeholder=" User name"/>
                </div>
                <div class='form-group'>
                    <label>Password</label>
                    <input id="password" type='password' class="form-control" placeholder=" Password"/>
                </div>
                <button type="submit" class="btn btn-success btn-block" id="loginBtn"> Login</button>
            </div>
        </div>
    </div>
</div>
    <body style="overflow-x: hidden;">
        <?php require('connection.php');
       // session_start();
        if($_SESSION['user']==null){
            echo "<h1>You are not Allowed to View page</h1>";
        }else{
        include("navigation.php");
        ?>
            <div class="row">
                <br>&nbsp;
                <br>&nbsp;
                <br>&nbsp;
                <br>
            </div>
            <div class="row col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="padding: 5px;">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-9">
                                <h3>Barcode Management</h3>
                            </div>
                            <div class="col-md-3">
                                <a id="modalBtn" class="btn btn-success pull-right btn-xs" style="margin-top: ">View all Department</a>
                            </div>
                            <div class="col-md-" style="padding-top:16px;">
                                <form action="view-generated.php" method="POST">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <?php
                                                require_once('barcodeC.php');
                                                $dept = json_decode(barcodeC::fetchallDept()); 
                                                ?>
                                                <select name="department" id="department_ID" class="form-control">
                                                <?php foreach($dept->payload as $key => $val){ ?>
                                                
                                                    <option value="<?= $val->DPT_ABBR; ?>"><?= $val->DPT_NAME; ?></option>
                                                
                                                <?php } ?>
                                                </select> 
                                            </div>
                                            <div class="col-lg-2">
                                                <!-- <a href="view-generated.php" id="generate" class="btn btn-primary" target="_blank">Generate</a> -->
                                                <input type="submit" name="submit" value="Generate" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <table class="table table-success table-hover table-responsive" id="batch-tbl">
                                <thead class="active">
                                    <th># </th>
                                    <th>Batch</th>
                                    <th>Dept</th>
                                    <th>Date</th>
                                    <th>View Batch</th>
                                </thead>
                                <tbody id="batch-body"></tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            </div>
            <?php }?>
    </body>
    <script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $.get("services/batch_tbl.php", function(data) {})
                .done(function(data) {
                    var receiveObject = $.parseJSON(data);
                    if (receiveObject.status === "success") {
                        var tblHtml = "";
                        var count = 0;
                        receiveObject.payload.forEach(function(receiveData) {
                            count++;
                            tblHtml += "<tr>" +
                                "<td>" + count + "</td>" +
                                "<td>" + receiveData.Batch + "</td>" +
                                "<td>" + receiveData.dept_name + "</td>" +
                                "<td>" + receiveData.Date + "</td>" +
                                "<td><a href='batchview.php?id=" + receiveData.Batch + "' class='btn btn-success btn-sm'>View</a></td>" +
                                "</tr>";
                        });
                        $("#batch-body").html(tblHtml);
                        $("#batch-tbl").DataTable();
                    }
                })
                .fail(function(qXHR, textStatus, errorThrown) {
                    console.log({
                        status: 'ERROR',
                        message: errorThrown
                    });
                })
                .always(function(data) {
                    // console.log("always - "+data);
                });

            $("#generate").mouseup(function() {
                setTimeout(
                    function() {
                        $.get("services/batch_tbl.php", function(data) {})
                            .done(function(data) {
                                var receiveObject = $.parseJSON(data);
                                if (receiveObject.status === "success") {
                                    var tblHtml = "";
                                    var count = 0;
                                    receiveObject.payload.forEach(function(receiveData) {
                                        count++;
                                        tblHtml += "<tr>" +
                                            "<td>" + count + "</td>" +
                                            "<td>" + receiveData.Batch + "</td>" +
                                            "<td>" + receiveData.Date + "</td>" +
                                            "<td><a href='batchview.php?id=" + receiveData.Batch + "' class='btn btn-success btn-sm'>View</a></td>" +
                                            "</tr>";
                                    });
                                    $("#batch-body").html(tblHtml);
                                    $("#batch-tbl").DataTable();
                                }
                            })
                    }, 5000);
            });
        });
    </script>

<script type="text/javascript">
    $("#modalBtn").unbind('click').on('click', function() {
        $("#myModal").modal()
        $("#loginBtn").unbind('click').on('click', function() {
            if ($("#user_name").val() == "admin" && $("#password").val() == "btradmin2k19") {
                $("#success").removeClass("hide")
                $("#failed").addClass("hide")
                setTimeout(function() {
                    window.location.href = "dept.php?"
                },600);
            } else {
                $("#failed").removeClass("hide")
                $("#success").addClass("hide")
            }
        })
    })
    </script>

    </html>