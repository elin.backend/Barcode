<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Barcode Management</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/full-width-pics.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/timeline.css" rel="stylesheet">
    <script src="js/jquery-1.12.3.js" type="text/javascript"></script>
    <script src="js/bootstrap.js" type="text/javascript"></script>  

</head>

<body class="main-body">
    <?php include('navigation.php');?>
        <div class="container">
        <?php 
        require_once('connection.php');
        session_start();
        if($_SESSION['user']==null){
            echo "<h1>You are not Allowed to View page</h1>";
        }
        else{
        include("navigation.php");
        ?>
        <div class="row"><br>&nbsp;<br>&nbsp;<br>&nbsp;<br></div>
        <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row"> 
                        <div class="col-md-8">
                        <h3>
                            <div class="row">
                            &nbsp;&nbsp;Batch #<?php 
                            $g = $_GET['id'];
                            echo $g."&nbsp;</div><div>Date:";
                            if(!empty($_GET['id'])){
                                $q = "SELECT * FROM uniquebar WHERE Batch='$g' LIMIT 1";
                                if($result = $conn->query($q)){
                                    if(mysqli_num_rows($result)){
                                    $row = mysqli_fetch_array($result);
                                    echo $row['Date']."</div>";
                                    }
                                }
                            }
                            
                            ?></h3>
                        </div>
                        <div class="col-md-2" style="padding-top:16px;">
                        <a href="home.php" class="btn btn-primary pull-right" >Back to Batches</a>
                        </div>
                        <div class="col-md-2" style="padding-top:16px;">
                        <a href="test.php?id=<?= $g?>" class="btn btn-info pull-left" target="_blank" >Re-Print</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-body">
                <div class="col-md-12">
                <table class="table table-condensed">
                    <thead>
                        <th>ID</th>
                        <th>Reference ID</th>
                        <th>Barcode</th>
                        <th>Dept</th>
                    </thead>
                    <tbody>

            <?php
            $iddd = mysqli_real_escape_string($conn,$_GET['id']);
            $count = $conn->query("SELECT count(*) as counter FROM uniquebar WHERE batch = '$iddd'"); 
            if(mysqli_num_rows($count)){
            $row = mysqli_fetch_array($count);
            $count = $row['counter'];   
            }
            $tbl_name="uniquebar";	
            $total_pages = $count;
            $tp = basename($_SERVER['REQUEST_URI']);
            $tp1 = explode('&',$tp);
            $targetpage = $tp1[0];
            $adjacents = 3;
            $limit = 15; 		
            $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 0;  
        if($page) 
            $start = ($page - 1) * $limit; 			
        else
            $start = 0;
            $sql = "SELECT * FROM $tbl_name WHERE batch = '$iddd' ORDER BY id ASC LIMIT $start, $limit";

            include('pagination2.php');
             if(mysqli_num_rows($result)>0){
                $i=1;
                while($row =mysqli_fetch_array($result)){
                ?>
                    <tr>
                        <td><?= $i++;?></td>
                        <td><?= $row['id'];?></td>
                        <td><?= $row['barcode'];?></td>
                        <td><?= $row['dept_name'];?></td>
                    </tr>
                <?php
                }
            }else{
                echo "<tr><td colspan=4>No Records Found</td></tr>";
            }            
            ?>
                    </tbody>
                </table>

            </div>
                </div>
                <div class="panel panel-footer">
                <div class="text-center">
                     <?php echo $pagination; ?>   
                </div> 
                </div>
          
            </div>
        </div>
        </div>
        <?php }?>
        </div>
    
</body>

</html>
