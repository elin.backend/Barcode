<html>
    <head>
        <meta charset="utf-8">
        <title>Batch View</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/full-width-pics.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="css/timeline.css" rel="stylesheet">
        <script src="js/jquery-1.12.3.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>  
        
    </head>
    <body>
        <?php 
        
        require_once('connection.php');
        
        ?>
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <label>Barcode Management</label>
                    <a href="testing.php" class="btn btn-info pull-right" target="_blank">Generate</a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <table class="table table-condensed table-responsive">
                            <thead>
                                <th>Batch</th>
                                <th>Date</th>
                                <th>View Batch</th>
                            </thead>
                            <tbody>

                    <?php

                    $count = $conn->query("SELECT distinct(Batch) FROM uniquebar");
                    $counter=0;
                    if(mysqli_num_rows($count)){
                        while($row = mysqli_fetch_array($count)){
                            $counter++;
                        }
                    }else{
                    $counter = 0;    
                    }
                    $tbl_name="uniquebar";	
                    $total_pages = $counter;
                    $targetpage = $_SERVER['PHP_SELF']; 
                    $adjacents = 3;
                    $limit = 15; 		
                    $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 0;  
                if($page) 
                    $start = ($page - 1) * $limit; 			
                else
                    $start = 0;
                    $sql = "SELECT * FROM $tbl_name GROUP BY Batch,Date ASC LIMIT $start, $limit";

                    include('pagination.php');
                     if(mysqli_num_rows($result)>0){
                        while($row =mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <td><?= $row['Batch'];?></td>
                                <td><?= $row['Date'];?></td>
                                <td><a href="batchview.php?id=<?= $row['Batch'];?>" class="btn btn-success">View</a></td>
                            </tr>
                        <?php
                        }
                    }else{
                        echo "<tr><td colspan=4>No Records Found</td></tr>";
                    }            
                    ?>
                            </tbody>
                        </table>
                        <div class="text-center">
                             <?php echo $pagination; ?>   
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>