<?php 
class barcodeC{
    
    public static function getRandom(){
        require('connection.php');
        $characters = '0123456789';
        $string = '';
        while($string ==''){
            for ($i = 0; $i < 13; $i++) {
                $string .= $characters[rand(0, strlen($characters) - 1)];  
            }
            $q = "SELECT * FROM uniquebar WHERE barcode = '$string'";
            $result = $conn->query($q);
            if($result->num_rows > 0){
                $string = '';
            }
            
        }
        return $string;
    }
    public static function getLastBatch(){
        require('connection.php');
        $q = "SELECT * FROM uniquebar ORDER BY id DESC LIMIT 1";
        $res = $conn->query($q);
        $row = $res->fetch_assoc();
        $vals = $row['Batch'];
        return $vals++;
    }
    public static function insertBarcode($val,  $batch,$dept_id){
        require('connection.php');
        $q = "INSERT INTO uniquebar VALUES(NULL,'$val', '$batch',NOW(), '$dept_id')";
        if($conn->query($q)){
            $s =  "Success";
        }else{
            $s = "Failed". $conn->error;
        }
        return $s;
    }
    public static function reprintCode($id){
        require('connection.php');
        $id  = mysqli_real_escape_string($conn,$id);
        $q = "SELECT * FROM uniquebar WHERE Batch= '$id'";
        $res = $conn->query($q);
        $array= array();
        if($res->num_rows){
            while($row = $res->fetch_assoc()){
                $data1 = array("barcode"=>$row['barcode'],
                                "dept" =>$row['dept_name']);
                array_push($array,$data1);
            }
        }else{
            $array = array();
        }
        return $array;
    }
    public static function fetchallDept(){
        require('connection.php');

        $data = array();
            if($record = mysqli_query($conn,"SELECT * FROM `department`")) {
            // if($record = mysqli_query($conn,"CALL all_users()")) {  //with stored proc

                while($row=mysqli_fetch_object($record)){
                    //  $data[] = array_map("utf8_encode",$row);
                    $data[] = $row;
                }

                $response = array('status' => 'SUCCESS',
                                    'message' => "Retrieving Data Success",
                                    'payload' => $data);
                return json_encode($response);
            }else{
                $response = array('status' => 'ERROR',
                                    'message' => 'FAIL Data FETCHING');
                return json_encode($response);
            }
    }

    public static function viewDeptByID(){
        $data = array();
        $payload = json_decode(file_get_contents('php://input'));
        
        if($record = mysqli_query($conn,"SELECT * FROM `department` WHERE DPT_ID = '".$payload->DPT_ID."'")) {
        // if($record = mysqli_query($conn," CALL user_ID('".$payload->user_id."')")) {   //with stored proc

            $data = mysqli_fetch_object($record);
            // echo json_encode( $data);

                if(mysqli_num_rows($record) > 0) {
                    $response = array('status' => 'SUCCESS',
                                'message' => "Retrieving Registration Success",
                                'payload' => $data);
                                // 'payload' => array_map("utf8_encode",mysqli_fetch_assoc($record)));
                    echo json_encode($response);
            }else{
                $response = array('status' => 'ERROR',
                                'message' => 'FAIL RETRIEVING REGISTRATION');
                echo json_encode($response);
            }
            
        }else{
            $response = array('status' => 'ERROR',
                                'message' => 'FAIL RETRIEVING User');
            echo json_encode($response);
        }
    
    }

    public static function upsertDept($payload){
        $payload = json_decode(file_get_contents('php://input'));
        if($record = mysqli_query($conn,"SELECT * FROM `department` WHERE DPT_NAME = '".$payload->dept_abbr."'")){
            if(mysqli_num_rows($record) > 0) {
                // $user = mysqli_fetch_object($record);
                $response = array('status' => 'SUCCESS',
                                    'message' => 'Already Exist'); 
                                    // 'payload' => $user->civil_id);
                echo json_encode($response);
                exit();
            }
        }
        $datenow = date("Y-m-d");
        // echo json_encode($datenow);
        $insert = mysqli_query($conn,"INSERT INTO `department` Set  
            DPT_NAME = '".$payload->dept_name."',
            DPT_ABBR = '".$payload->dept_abbr."'
            ");
     // echo mysqli_error($conn);
        if($insert){
            $response = array('status' => 'SUCCESS',
                            'message' => 'INSERTING DATA SUCCESS',
                            'DPT_ID' => mysqli_insert_id($conn));
            echo json_encode($response);
        }else{
            $response = array('status' => 'ERROR',
                                'message' => 'INSERTING DATA FAIL');
            echo json_encode($response);
        }
    
    
    }

    
    
}

?>