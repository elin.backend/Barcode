 <nav class="navbar navbar-fixed-top" role="navigation" style="background-color:white;height:80px; border-bottom:solid 1px #BBB;" >
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-md-10" >               
                <a class="navbar-brand nav-logo" href="home.php"><img alt="Brand" src="images/logo2.png" ></a>
                <a class="navbar-brand nav-text" href="home.php"><p>Bureau of the Treasury</p> Barcode Management</a>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-top:15px;">
                <ul class="nav navbar-nav pull-left">
                    <li class="active dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Admin <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li>
                            <a href="cp.php"><i class="glyphicon glyphicon-lock"></i> Change Password </a>
                            </li>
                            <li>
                            <a href="logout.php"><i class="glyphicon glyphicon-log-out"></i> Logout </a>  </li>
                          </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            
        </div>
        <!-- /.container -->
    </nav>
