<?php
header("Content-type:application/json");
date_default_timezone_set("Asia/Manila");
$servername = "localhost";
$username = "root";
$password = "p@ssw0rd";
$db = "bureauoftreasury";

// Create connection
$conn = mysqli_connect($servername, $username, $password,$db);
// echo $conn;

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
// echo "Connected successfully";

$action = $_GET['action'];

//echo $action;

switch ($action) {
	case 'viewall':
	// echo 'test';
	$data = array();
	if($record = mysqli_query($conn,"SELECT * FROM `department`")) {
	// if($record = mysqli_query($conn,"CALL all_users()")) {  //with stored proc

		while($row=mysqli_fetch_object($record)){
			//  $data[] = array_map("utf8_encode",$row);
			 $data[] = $row;
		}

		$response = array('status' => 'SUCCESS',
							'message' => "Retrieving Data Success",
							'payload' => $data);
		echo json_encode($response);
	}else{
		$response = array('status' => 'ERROR',
							'message' => 'FAIL Data FETCHING');
		echo json_encode($response);
	}
	break;

// view

	case 'view':
	
	// echo 'test';
	$data = array();
	$payload = json_decode(file_get_contents('php://input'));
	
	if($record = mysqli_query($conn,"SELECT * FROM `department` WHERE DPT_ID = '1'")) {
	// if($record = mysqli_query($conn," CALL user_ID('".$payload->user_id."')")) {   //with stored proc

		$data = mysqli_fetch_object($record);
		// echo json_encode( $data);

			if(mysqli_num_rows($record) > 0) {
				$response = array('status' => 'SUCCESS',
							'message' => "Retrieving Registration Success",
							'payload' => $data);
							// 'payload' => array_map("utf8_encode",mysqli_fetch_assoc($record)));
				echo json_encode($response);
		}else{
			$response = array('status' => 'ERROR',
							'message' => 'FAIL RETRIEVING REGISTRATION');
			echo json_encode($response);
		}
		
	}else{
		$response = array('status' => 'ERROR',
							'message' => 'FAIL RETRIEVING User');
		echo json_encode($response);
	}

	break;
	
//insert
	case 'insert':
	$payload = json_decode(file_get_contents('php://input'));
	if($record = mysqli_query($conn,"SELECT * FROM `department`  WHERE DPT_NAME = '".$payload->DPT_NAME."'")){
		if(mysqli_num_rows($record) > 0) {
			// $user = mysqli_fetch_object($record);
			$response = array('status' => 'SUCCESS',
								'message' => 'Already Exist'); 
								// 'payload' => $user->civil_id);
			echo json_encode($response);
			exit();
		}
	}
	$datenow = date("Y-m-d");
	// echo json_encode($datenow);
	$insert = mysqli_query($conn,"INSERT INTO `department` Set  
		DPT_NAME = '".$payload->DPT_NAME."',
		DPT_ABBR = '".$payload->DPT_ABBR."'
		");
 // echo mysqli_error($conn);
	if($insert){
		$response = array('status' => 'SUCCESS',
						'message' => 'INSERTING DATA SUCCESS',
						'DPT_ID' => mysqli_insert_id($conn));
		echo json_encode($response);
	}else{
		$response = array('status' => 'ERROR',
							'message' => 'INSERTING DATA FAIL');
		echo json_encode($response);
	}
	break;

// update

	case 'update':
	$payload = json_decode(file_get_contents('php://input'));
	if($record = mysqli_query($conn,"SELECT * FROM `department` WHERE DPT_NAME = '".$payload->DPT_NAME."' ")){
		if(mysqli_num_rows($record) == 0) {
			$user = mysqli_fetch_object($record);
			$response = array('status' => 'ERROR',
								'message' => 'Not Exist');

		echo json_encode($response);
			exit();
		}
	}
	$update = mysqli_query($conn, "UPDATE `department` Set
				DPT_NAME = '".$payload->DPT_NAME."',
				DPT_ABBR = '".$payload->DPT_ABBR."'
		WHERE   DPT_ID = '".$payload->DPT_ID."'
					");
		
	if($update){
		$response = array('status' => 'SUCCESS',
							'message' => 'UPDATE SUCCESS'
							);
		echo json_encode($response);
	}else{
		$response = array('status' => 'ERROR',
							'message' => 'UPDATE FAIL',
							'error_code' => mysqli_error($conn));
		echo json_encode($response);
	}
	break;

}
?>