<!DOCTYPE html>
<html>
<head>
	<title>Document Tracking System</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link href="../css/error.css" rel="stylesheet">
</head>
<body class="error-body">
	<div class="error-container">
		<div class="error-left">
		</div>
		<div class="error-right">
			<div class="right-top">
				<h2>Page Restricted!</h2>
			</div>
			<div class="right-bottom">
				<pre>You are trying to access a forbidden web address which 
you don't have enough permission.

By accessing this page you IP Address will be listed.</pre>
			</div>
		</div>
	</div>
</body>
</html>